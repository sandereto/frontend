import React, { useState } from "react";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";

// core components
import GridContainer from "components/Grid/GridContainer.js";
import GridItem from "components/Grid/GridItem.js";

import styles from "assets/jss/material-kit-react/views/landingPageSections/productStyle.js";
import CustomInput from "components/CustomInput/CustomInput";

const useStyles = makeStyles(styles);

export default function CampoCalculavel() {
  const classes = useStyles();
  const [valor, setValor] = useState("");
  // Aceita apenas números e Símbolos
  const maskOnlyNumbers = (value) => {
    return value.replace(/[a-zA-Z]/gi, "");
  };
  //Função que pega o change do campo e salva no estado
  const handleCahnge = (e) => {
    setValor(maskOnlyNumbers(e.target.value));
  };
  //Função que calcula a operação
  const calc = () => {
    try {
      let soma =
        eval(valor.replace("=", "")) === Infinity
          ? 0
          : eval(valor.replace("=", ""));
      setValor(soma);
    } catch {
      return [alert("ERRO DE OPERAÇÃO"), setValor(0)];
    }
  };
  //Função que pega verifica o evento da tecla enter e calcula a operação
  const enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      calc();
    }
  };
  return (
    <div className={classes.section} style={{ marginTop: "40px" }}>
      <GridContainer justify="left">
        <GridItem xs={12} sm={12} md={12}>
          <CustomInput
            label="CAMPO CALCULÁVEL"
            value={valor}
            onChange={handleCahnge}
            onKeyPress={enterPressed}
          />
        </GridItem>
      </GridContainer>
    </div>
  );
}
