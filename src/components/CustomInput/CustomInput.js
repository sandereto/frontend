import React from "react";
import TextField from "@material-ui/core/TextField";
import PropTypes from "prop-types";

export default function CustomInput(props) {
  return (
    <TextField
      id={props.id}
      label={props.label}
      value={props.value}
      onChange={props.onChange}
      variant={props.variant}
      onKeyPress={props.onKeyPress}
    />
  );
}

CustomInput.propTypes = {
  id: PropTypes.string,
  label: PropTypes.string,
  value: PropTypes.value,
  onChange: PropTypes.func,
  onKeyPress: PropTypes.func,
  variant: PropTypes.string,
};
